# Exp ISDuBA

This is an experimental tech play ground for the ISDuBA project.

## License

- `expisduba` is licensed as Free Software under MIT License.

- See the specific source files
  for details, the license itself can be found in the directory [LICENSES](./LICENSES).
