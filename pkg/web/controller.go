// This file is Free Software under the MIT License
// without warranty, see README.md and LICENSES/MIT.txt for details.
//
// SPDX-License-Identifier: MIT
//
// SPDX-FileCopyrightText: 2024 German Federal Office for Information Security (BSI) <https://www.bsi.bund.de>
// Software-Engineering: 2024 Intevation GmbH <https://intevation.de>

// Package web implements the endpoints of the web server.
package web

import (
	"log/slog"
	"net/http"

	"github.com/gin-gonic/gin"
	sloggin "github.com/samber/slog-gin"
	"heptapod.host/intevation/expisduba/pkg/config"
	"heptapod.host/intevation/expisduba/pkg/database"
	"heptapod.host/intevation/expisduba/pkg/ginkeycloak"
)

// Controller binds the endpoints to the internal logic.
type Controller struct {
	cfg *config.Config
	db  *database.DB
}

// NewController returns a new Controller.
func NewController(cfg *config.Config, db *database.DB) *Controller {
	return &Controller{cfg: cfg, db: db}
}

// Bind return a http handler to be used in a web server.
func (c *Controller) Bind() http.Handler {
	r := gin.New()
	r.Use(sloggin.New(slog.Default()))
	r.Use(gin.Recovery())

	kcCfg := c.cfg.Keycloak.Config(extractTLPs)

	authRoles := func(roles ...string) gin.HandlerFunc {
		return ginkeycloak.Auth(ginkeycloak.RoleCheck(roles...), kcCfg)
	}

	var (
		authIm     = authRoles("importer")
		authBeReAu = authRoles("bearbeiter", "reviewer", "auditor")
	)

	api := r.Group("/api")
	api.POST("/documents", authIm, c.importDocument)
	api.GET("/documents", authBeReAu, c.overviewDocuments)
	api.GET("/documents/:id", authBeReAu, c.viewDocument)

	return r
}
